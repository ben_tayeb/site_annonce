<!DOCTYPE html>
<html lang="fr">
	<head>
   	 	<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    	<link rel="stylesheet" href = "style/style.css">
    	<title>La bonne affaire</title>
	</head>
	<body>
		<header>
			<h1> La Bonne Affaire </h1>
			<div class="menu">
				<ul>
					<li><a href="index.php"> Accueil</a></li>
					<li><a href="#">Vendre</a></li>
					<li><a href="connexion.php">Connexion</a></li>
					<li><a href="#">Contact</a></li>
				</ul>
			</div>
		</header>	
		<form action="/ma-page-de-traitement" method="post">
   		 	<div>
        		<label for="name">Nom :</label>
        		<input type="text" id="name" name="user_name">
    		</div>
    		<div>
        		<label for="mail">e-mail :</label>
        		<input type="email" id="mail" name="user_mail">
    		</div>
    		<div>
        		<label for="msg">Message :</label>
        		<textarea id="msg" name="user_message"></textarea>
    		</div>
		</form>
	</body>
</html>
