<?php

include "db/config.php";

// On s'assure que l'email et le mot de passe sont bien définis
if ((!empty($_POST["email"])) && (!empty($_POST["motdepasse"]))) {
	// on recupere l'email entré par l'utilisateur
    $email = $_POST["email"];
	// on hache le mdp
    $mdp = hash('sha256', $_POST["motdepasse"]);
	
	// On cherche dans la table utilisateur si il existe un utilisateur avec ce mail et ce mdp 
	// on selectionne tout de la table utilisateurs où email et le mdp sont egales au donnes entré par l'utilisateur
	// on prepare pour eviter les injection SQL
    $reqResultatUtilisateur = $pdo->prepare("
        SELECT * 
        FROM utilisateurs 
        where email = :email
        and motdepasse = :mdp
    ");
	
    $reqResultatUtilisateur->bindParam(':email', $email); // :email prend la valeur de $email
    $reqResultatUtilisateur->bindParam(':mdp', $mdp);

    $reqResultatUtilisateur->execute(); // on execute la requete 
    $resultat = $reqResultatUtilisateur->fetch(); // fetch nous retourne une ligne ou rien

    if ($resultat) { // si on nous retourne une ligne ou $resultat est vrai alors on start la session et on recupere l'id de l'utilisateur et on la stocke dans la session, on est redirigé vers pagePrincipale.php
        session_start();
        $_SESSION["id"] = $resultat["id"];
        header("Location: pagePrincipale.php");
    } else {
        echo "Erreur de connexion";
    }
} else {
    echo "Les champs <b>email</b> et <b>mot de passe</b> sont obligatoires.";
}





