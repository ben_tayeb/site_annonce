<?php
// configuration et création de la connexion à la base de données

$servername = "localhost";
$username = "root";
$password = "";

try {
    $pdo = new PDO("mysql:host=$servername;dbname=La_bonne_affaire", $username, $password);
    // Activation des erreurs en mode exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
} catch(PDOException $e) {
    echo "Connexion bdd a échoué: " . $e->getMessage();
}
