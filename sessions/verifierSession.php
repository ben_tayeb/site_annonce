<?php
    // Verifie si la session utilisateur est toujours valide
    // si cette dernière n'est pas valide, l'utlisateur est redirigé vers la page de connexion
    session_start();
    $idUtilisateur = $_SESSION["id"];

    
    if (!isset($idUtilisateur)) {
        header("Location: index.php");
    }
    
