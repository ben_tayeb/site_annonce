<?php

include "db/config.php";
include "sessions/verifierSession.php";

if (!empty($_POST["email"])&& !empty($_POST["nom"])&& !empty($_POST["prenom"])&& !empty($_POST["motdepasse"])) {
    $email = $_POST["email"];
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    // Nous utilisons une fonction de hash pour ne pas enregistrer le mot de passe en clair
    $mdp = hash('sha256', $_POST["motdepasse"]);

    // on vérifie si l'email ne correspond pas déjà à un compte éxistant 
    // en excluant le compte de l'utilisateur courant
    //on prepare la requete pour eviter les injections SQL
    // On selectionne tout de la table utilisateurs où l'email = $email et où l'id est different de l'id de l'utilisateur courant
    $reqResultatUtilisateur = $pdo->prepare("	 
        SELECT * 				
        FROM utilisateurs
        WHERE email = :email
        AND id != :idUtilisateur
    "); 
    $reqResultatUtilisateur->bindParam(':email', $email); // :email prend la valeur $email
    $reqResultatUtilisateur->bindParam(':idUtilisateur', $idUtilisateur);

    $reqResultatUtilisateur->execute();
    $emailExiste = $reqResultatUtilisateur->fetch();

    if ($emailExiste) { /* si la valeur n'est ni 'null' ni ' "" ' ou ni 'false' */ // si il trouve un utilisateur avec le même email
        echo "Désolé cet email est déjà reliée à un compte.";
    } else {
        $reqModifierCompte = $pdo->prepare("
            UPDATE utilisateurs
	    	SET email= :email, nom= :nom, prenom= :prenom, motdepasse= :mdp
			WHERE id = :idUtilisateur
        ");
        // utilisation de la fonction 'bindParam' pour eviter les injections
        $reqModifierCompte->bindParam(':idUtilisateur', $idUtilisateur);
        $reqModifierCompte->bindParam(':email', $email);
        $reqModifierCompte->bindParam(':nom', $nom);
        $reqModifierCompte->bindParam(':prenom', $prenom);
        $reqModifierCompte->bindParam(':mdp', $mdp);

        $reqModifierCompte->execute();

        if ($reqModifierCompte) { // si il ya un resultat ou si $reqModifierCompte est vrai
            echo "Merci votre compte a bien été modifiée !";
			echo "</br>retour à la page <a href='pagePrincipale.php'>principale </a>";            
        } else {
            echo "Erreur lors de la modification de votre compte !";
        }
    }    
} else {
    echo "Veuillez renseigné tout les champs";
}
