<?php
	include "sessions/verifierSession.php";
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
    	<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    	<link rel="stylesheet" href = "style/style.css">
    	<title>La bonne affaire</title>
	</head>
	<body>
		<header>
			<h1> La Bonne Affaire </h1>
			<div class="menu">
				<ul>
					<li><a href="pagePrincipale.php">Accueil</a></li>
				</ul>
			</div>
		</header>
		<div class="inscription">
			<form action="validerCreationAnnonce.php" method="post">
				<h3>Créer votre annonce</h3>
				<label for="titre"> Titre de l'annonce: </label><input type="text" name="titre" required><br>
				<label for="titre"> Description de l'annonce: </label><textarea rows="5" cols="26" name="description" required></textarea><br>
				<label for="prix">Prix:</label> <input type="number" name="prix" required><br>
				<input type="submit" value="Envoyer">
			</form>
		</div>   
	</body>
</html>
