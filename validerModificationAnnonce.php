<?php
	include "db/config.php";
	include "sessions/verifierSession.php";
// Si l'id de l'annonce, titre.... ne sont pas vides 
	if (!empty($_POST["idAnnonce"]) && !empty($_POST["titre"]) && !empty($_POST["description"]) && !empty($_POST["prix"])) {		
	// On recupere l'id de l'annonce
		$idAnnonce = $_POST['idAnnonce'];
		// on recupere les données entrés par l'utilisateur
		$titre = $_POST['titre'];
		$description = $_POST['description'];
		$prix = $_POST['prix'];
	// On prepare pour eviter les injections SQL 
    // On modifie le titre, la description et le prix par les donnés entrés par l'utilsateur
		$reqModifierAnnonce = $pdo->prepare("
			UPDATE annonces
			SET titre = :titre, description_texte = :description, prix = :prix
			WHERE id = :idAnnonce
			AND id_utilisateur = :idUtilisateur
		");
		// bindParam pour eviter les injections SQL 
		$reqModifierAnnonce->bindParam(':idUtilisateur', $idUtilisateur);
		$reqModifierAnnonce->bindParam(':idAnnonce', $idAnnonce);
		$reqModifierAnnonce->bindParam(':titre', $titre);
		$reqModifierAnnonce->bindParam(':description', $description);
		$reqModifierAnnonce->bindParam(':prix', $prix);

		$reqModifierAnnonce->execute(); // on execute la requete
		
		if ($reqModifierAnnonce) { // si il y a une $reqModifierAnnonce ou si $reqModifierAnnonce est vrai 
			header("Location: mesAnnonces.php");    // on est redirigé vers la page mesAnnonces.php  
		} else {
			echo "Erreur lors de la suppression de votre annonce !";
		}
 	} else {
		echo "Veuillez renseigner tout les champs";
	}	
?>
