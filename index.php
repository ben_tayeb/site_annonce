<?php
    include "db/config.php";
	// On selectionne l'id de l'annonce, l'email de l'utilisateur, la description de l'annonce, le prix et la date en rejoignant la table annonces et utilisateurs grace l'id de l'utilisateur présent dans les 2 tables
    $reqAnnonces = $pdo->query("
        SELECT a.id, u.email, a.titre, a.description_texte, a.prix, a.date_creation
        FROM annonces a
        INNER JOIN utilisateurs u ON u.id = a.id_utilisateur ORDER BY date_creation DESC
    ");
    $annonces = $reqAnnonces->fetchAll(); // retourne un tableau avec toutes les annonces
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style/style.css">
    <title>La bonne affaire</title>
</head>
<body>
	<header>
		<h1> La Bonne Affaire </h1>
		<div class="menu">
			<ul>
				<li><a href="connexion.php">Connexion</a></li>
			</ul>
		</div>
	</header>    
    <div class="annonces">
        <?php
            foreach($annonces as $annonce) { // Pour chaque annonces on affichera le titre, le prix, la description et la date de creation et une image 
        ?>
            <div class="annonce">
                <div class="annonceHeader">
                    <h2 class="annonceTitre"><?php echo $annonce['titre']; ?></h2>
                    <h3 class="annoncePrix"><?php echo $annonce['prix']; ?> €</h3>
                </div>
                    <div class="div">
                        <img class="annonceImage" src="images/voiture_exemple.jpg" alt="voiture exemple" height="100px">
                        <p class="annonceTexte">
                            <?php echo $annonce['description_texte']; ?>
                        </p>
			            <h4> <?php echo $annonce['date_creation']; ?> </h4>
                    </div>
            </div>
        <?php
            }
        ?>        
    </div>
</body>
</html>
