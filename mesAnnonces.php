<?php
	include "db/config.php";
	include "sessions/verifierSession.php";
	// On selectionne tout de l'utilisateur connecté;
	$reqUtilisateurCourant = $pdo->query("
		SELECT *
		FROM utilisateurs
		WHERE $idUtilisateur = id
	");
	$utilisateurCourant = $reqUtilisateurCourant->fetch();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="style/style.css">
	<title>La bonne affaire</title>
</head>
<body> 
	<header>
		<h1> La Bonne Affaire </h1>
		<div class="menu">
			<ul>
				<li><a href="pagePrincipale.php">Accueil</a></li>
				<li><a href="modifierCompte.php"><?php echo $utilisateurCourant['nom']." ".$utilisateurCourant['prenom']; ?></a></li>					
			</ul>
		</div>
	</header>
	<?php    
		//On selectionne l'id de l'annonce, l'email de l'utilisateur, la description de l'annonce, le prix et la date en rejoignant la table annonces et utilisateurs grace l'id de l'utilisateur présent dans les 2 tables où l'id est egal à l'id de l'utilisateur connecté
		// On va recuperer en gros tout les annonces de l'utilisateur connecté
		$reqAnnoncesUtilisateurCourant = $pdo->query("
			SELECT a.id, u.email, a.titre, a.description_texte, a.prix, a.date_creation
			FROM annonces a
			INNER JOIN utilisateurs u ON u.id = a.id_utilisateur
			WHERE a.id_utilisateur = $idUtilisateur
			ORDER BY date_creation DESC
		");
		$annoncesUtilisateurCourant = $reqAnnoncesUtilisateurCourant->fetchAll();
	?>
	<table style="width: 100%;">
		<tr>
			<th colspan="3" class="titreTableauAnnonces">Mes annonces</th>
		</tr>
		<tr>
			<th>Titre de l'annonce</th>
			<th>Date de creation</th>
			<th>Description de l'annonce</th>
			<th>Actions</th>
		</tr>
		<?php
			foreach($annoncesUtilisateurCourant as $annonce) { // Pour chaque annonce de l'utilisateur connecté, on affiche le titre, la date et la description
		?>		
			<tr>
				<td><?php echo $annonce['titre']?></td>
				<td><?php echo $annonce['date_creation']?></td>
				<td><?php echo $annonce['description_texte']?></td>
				<td>
					<form action="modifierAnnonces.php" method="post">
						<input name="idAnnonce" type="hidden" value="<?php echo $annonce['id'];?>" /><br/> <!--  On cache un input pour pouvoir recuperer dans le fichier modifierAnnonces.php l'id de l'annonce ainsi on pourra modifier l'annonce dans ce fichier-->
						<button type="submit">Modifier</button> <!--quand on appuie sur modifier le fichier modifierAnnonces.php s'actionne ---->
					</form>
					<form action="validerSupprimerAnnonce.php" method="post">
						<input name="idAnnonce" type="hidden" value="<?php echo $annonce['id'];?>" /><br/> <!--  On cache un input pour pouvoir recuperer dans le fichier validerSupprimerAnnonce.php l'id de l'annonce ainsi on pourra supprimer l'annonce dans ce fichier-->
						<button type="submit">Supprimer</button><!--quand on appuie sur modifier le fichier validerSupprimerAnnonce.php s'actionne ---->
					</form>
				</td>
			</tr>
		<?php
			}
		?> 
	</table>
</body>
</html>
