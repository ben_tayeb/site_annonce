<?php
	include "db/config.php";
  include "sessions/verifierSession.php";
?>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href = "style/style.css">
	<title>La bonne affaire</title>
</head>
<body>
	<header>
		<h1> La Bonne Affaire </h1>
		<div class="menu">
			<ul>
				<li><a href="pagePrincipale.php">Accueil</a></li>
				<li><a href="mesAnnonces.php">Mes annonces</a></li>
			</ul>
		</div>
	<header>
  <?php
    if (!empty($_POST["idAnnonce"])) {
	// On recupere l'id de l'annonce que l'on avait caché dans le input de la mesAnnonces.php
      $idAnnonce = $_POST['idAnnonce'];
	  //On prepare pour eviter les injections SQL
	  //On selectionne l'annonce de l'utilisateur grace a l'id de l'annonce
      $reqAnnonce = $pdo->prepare("
        SELECT *
        FROM annonces
        WHERE id_utilisateur = :idUtilisateur
        AND id = :idAnnonce
      ");
	
      $reqAnnonce->bindParam(':idAnnonce', $idAnnonce);
      $reqAnnonce->bindParam(':idUtilisateur', $idUtilisateur);

      $reqAnnonce->execute(); // on execute la requete
      $resultatReqAnnonce = $reqAnnonce->fetch(); // on retourne soit une ligne, soit rien
      
      if ($resultatReqAnnonce) { // si il y a une ligne ou si $resultatReqAnnonce est vrai alors on pourra modifier l'annonce
  ?>
        <div class="inscription">
          <form action="validerModificationAnnonce.php" method="post">
            <h3>Modifier votre annonce</h3>
            <label for="titre"> Titre de l'annonce: </label><input type="text" name="titre" value="<?php echo $resultatReqAnnonce["titre"] ?>" required><br>
            <label for="titre"> Description de l'annonce: </label><textarea rows="5" cols="26" name="description" required><?php echo $resultatReqAnnonce["description_texte"] ?></textarea><br>
            <label for="prix">Prix:</label><input type="number" name="prix" value="<?php echo $resultatReqAnnonce["prix"] ?>" required><br>
            <input type="hidden" name="idAnnonce" value="<?php echo $resultatReqAnnonce["id"] ?>" required><br><!--  On cache un input pour pouvoir recuperer dans le fichier validerModificationAnnonce.php l'id de l'annonce ainsi on pourra modifier l'annonce dans la base de donnés---->
            <input type="submit" value="Modifier">
          </form>
        </div>
  <?php
      } else { // sinon 
        echo "Erreur annonce inexistante !";
      }
    } else { // si il n'y a pas de id de l'annonce
      echo "Erreur annonce inexistante !";     
    }	
  ?>	     
</body>
</html>

