<?php
	include "db/config.php";
	include "sessions/verifierSession.php";
	// On selectionne tout de l'utilisateur connecté c'est à dire id,nom,prenom,email,mdp
	$reqResultatUtilisateur = $pdo->query("
		SELECT *
		FROM utilisateurs
		WHERE id = $idUtilisateur
	");
	
	$utilisateur = $reqResultatUtilisateur->fetch(); // fetch nous retourne la ligne où il y a l''id,le nom, le prenom, l'email, le mdp de l'utilisateur connecté
?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<link rel="stylesheet" href = "style/style.css">
		<title>La bonne affaire</title>
	</head>
	<body>
		<header>
			<h1> La Bonne Affaire </h1>
			<div class="menu">
				<ul>
					<li><a href="pagePrincipale.php"> Accueil</a></li>
					<li><a href="mesAnnonces.php">Mes annonces</a></li>
				</ul>
			</div>
		</header>
		<div class="inscription">
			<form action="validerModificationCompte.php" method="post">
				<h3>Modifier votre compte <?php echo $utilisateur["nom"]." ".$utilisateur["prenom"]?></h3>
				<label for="email">Email: </label><input type="email" name="email" value="<?php echo $utilisateur["email"]; ?>"required><br>
				<label for="nom">Nom: </label><input type="text" name="nom" value="<?php echo $utilisateur["nom"]; ?>" required><br>
				<label for="prenom">Prénom: </label><input type="text" name="prenom" value="<?php echo $utilisateur["prenom"]; ?>" required><br>
				<label for="mdp">Mot de passe:</label> <input type="password" name="motdepasse" placeholder="Entrez un nouveau mot de passe" required><br>
				<input type="submit" value="Modifier">
			</form>
		</div>  
	</body>
</html>
