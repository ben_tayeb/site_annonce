<?php

include "db/config.php";
include "sessions/verifierSession.php";
// Si les champs titre, description et prix ne sont pas vides
if (!empty($_POST["titre"]) && !empty($_POST["description"]) && !empty($_POST["prix"])) {
    $titre = $_POST["titre"];
    $description = $_POST["description"];
    $prix = $_POST["prix"];

    // on vérifie si le titre et la description de l'annonce ne correspond pas déjà à une annonce éxistante
    $resultatRequete = $pdo->prepare('SELECT * FROM annonces WHERE titre = :titre AND description_texte = :description_texte');
    $resultatRequete->bindParam(':titre', $titre);
    $resultatRequete->bindParam(':description_texte', $description);

    $resultatRequete->execute();
    $annonceExiste = $resultatRequete->fetch(); // fetch retourne une seule ligne ou rien

    if ($annonceExiste) { /* si la valeur n'est ni 'null' ou ni ' "" ' ou ni 'false' */  // si il retourne une ligne ou si $annonceExiste est vrai
        echo "Désolé une annonce similaire existe déjà";
    } else {
		//On prepare la requete pour eviter les injections SQL
		// on insert dans la table annonces les données entrés par l'utilisateurs 
        $resultatRequete = $pdo->prepare("
            INSERT INTO annonces (id_utilisateur, titre, description_texte, prix)
            VALUES (:id_utilisateur, :titre, :description_texte, :prix)
        ");
        // utilisation de la fonction 'bindParam' pour eviter les injections
        $resultatRequete->bindParam(':id_utilisateur', $idUtilisateur);
        $resultatRequete->bindParam(':titre', $titre);
        $resultatRequete->bindParam(':description_texte', $description);
        $resultatRequete->bindParam(':prix', $prix);

        $resultatRequete->execute(); // on execute la requete

        if ($resultatRequete) {  // si $resultatRequete est vrai ou si il ya un $resultatRequete alors
            echo "Merci votre annonce a été créé !</br>";
			echo "retour à la page <a href='pagePrincipale.php'>principale </a>";		
            
        } else { // sinon
            echo "Erreur lors de la création de votre annonce !";
        }
    }
}  else {// Si tous les champs ne sont pas remplis
    echo "Veuillez renseigner les champs titre, description et prix.";
}

