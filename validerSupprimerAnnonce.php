<?php
	include "db/config.php";
	include "sessions/verifierSession.php";
// si l'id de l'annonce n'est pas vide ou si il y a un id de l'annonce alors
	if (!empty($_POST["idAnnonce"])) {	
		// On stocke l'id de l'annonce recupere grace à l'input caché de la page mesAnnonces.php 
		$idAnnonce = $_POST['idAnnonce'];
		// On prepare pour eviter les injections
		// On supprime tout de l'annonce de l'utilisateur connecté
		$reqSupprimerAnnonce = $pdo->prepare("
			DELETE
			FROM annonces
			WHERE id_utilisateur = :idUtilisateur
			AND id = :idAnnonce
		");
		// Eviter les injection SQL
		$reqSupprimerAnnonce->bindParam(':idAnnonce', $idAnnonce);
		$reqSupprimerAnnonce->bindParam(':idUtilisateur', $idUtilisateur);

		$reqSupprimerAnnonce->execute();
		
		if ($reqSupprimerAnnonce) { // si $reqSupprimerAnnonce est vrai 
			echo "Votre annonce a bien été supprimée !";
			echo "</br>retour à la page <a href='pagePrincipale.php'>principale </a>";            
		} else {
			echo "Erreur lors de la suppression de votre annonce !";
		}
 	} else {
		echo "Erreur lors de la suppression de votre annonce !";
	}	
?>
