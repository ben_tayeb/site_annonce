<!DOCTYPE html>
<html lang="fr">
	<head>
    	<meta charset="UTF-8">
    	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    	<meta http-equiv="X-UA-Compatible" content="ie=edge">
    	<link rel="stylesheet" href = "style/style.css">
    	<title>La bonne affaire</title>
	</head>
	<body>
		<header>
			<h1> La Bonne Affaire </h1>
			<div class="menu">
				<ul>
					<li><a href="index.php">Acceuil</a></li>
					<li><a href="connexion.php">Connexion</a></li>
				</ul>
			</div>
		</header>
		<div class="inscription">
			<form action="validerInscription.php" method="post">
				<h3>Inscription</h3>
				<label for="email"> Email: </label><input type="email" name="email" required><br>
				<label for="nom"> Nom: </label><input type="text" name="nom" required><br>
				<label for="prenom">Prénom: </label><input type="text" name="prenom" required><br>
				<label for="mdp">Mot de passe:</label> <input type="password" name="motdepasse" required><br>
				<input type="submit" value="Envoyer">
			</form>
		</div>   
	</body>
</html>
