<?php

include "db/config.php";
// Si l'email, le nom,le prenom et le mdp est bien entrée (n'est pas vide)''
if (!empty($_POST["email"]) && !empty($_POST["nom"]) && !empty($_POST["prenom"])&& !empty($_POST["motdepasse"])) {
	// On recupere les données recuperes par l'utilisateur grace à $_POST
    $email = $_POST["email"];
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    // Nous utilisons une fonction de hash pour ne pas enregistrer le mot de passe en clair
    $mdp = hash('sha256', $_POST["motdepasse"]);

    // on vérifie si l'email ne correspond pas déjà à un compte éxistant
    $resultatRequete = $pdo->prepare('SELECT * FROM utilisateurs WHERE email = :email');
    $resultatRequete->bindParam(':email', $email);

    $resultatRequete->execute();
    $emailExiste = $resultatRequete->fetch(); // fetch retourne une seule ligne, elle retourne une ligne si l'email était déjà utilisé par un autre utilisateur

    if ($emailExiste) { /* si la valeur n'est ni 'null' ou ni ' "" ' ou ni 'false' */ // si $emailExiste est vrai ou si l'email existe déjà 
        echo "Désolé cet email est déjà reliée à un compte.";
    } else {
		// on prepare la requete pour eviter les injections SQL
        $resultatRequete = $pdo->prepare("
            INSERT INTO utilisateurs (email, nom, prenom, motdepasse)
            VALUES (:email, :nom, :prenom, :mdp)
        ");
        // utilisation de la fonction 'bindParam' pour eviter les injections
        $resultatRequete->bindParam(':email', $email); // :email prend la valeur $email
        $resultatRequete->bindParam(':nom', $nom); // :nom "" " " " " " " " " " " $nom
        $resultatRequete->bindParam(':prenom', $prenom);
        $resultatRequete->bindParam(':mdp', $mdp);

        $resultatRequete->execute();

        if ($resultatRequete) { // si il y a un $resultatRequete ou si $resultatRequete existe
            echo "Merci votre compte a été créé !";
			echo "</br>retour à la page <a href='pagePrincipale.php'>principale </a>";
            
        } else { 
            echo "Erreur lors de la création de votre compte !";
        }
    }    
} else { // Si tout les champs ne sont pas entrés
    echo "Veuillez renseigné tout les champs";
}
